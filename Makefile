.PHONY: test
test:
	go run main.go generate --component-path "gitlab.com/av1o/components-docgen/test@~latest" --template ./cmd/testdata/doc.tpl.md --footer ./cmd/testdata/footer.md ./cmd/testdata/template.yml > test.md
