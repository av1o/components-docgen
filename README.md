# Components Docgen

This application is a small CLI that generates markdown documentation for GitLab CI Components.

## Usage

Download the [latest release](https://gitlab.com/av1o/components-docgen/-/releases) and install it.

Run the application:

```shell
docgen generate \
    --component-path "gitlab.example.com/components/my-components" \
    --template "./doc.tpl.md" \
    "./templates/mycomponent/template.yml" > README.md
```

This will generate a README.md file that contains information about the component.
The `--template` flag instructs the application to inject additional information into the README, such as usage information.

An example template can be found [here](./cmd/testdata/doc.tpl.md)
