package cmd

import (
	"bytes"
	_ "embed"
	"fmt"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	"gitlab.com/av1o/components-docgen/internal/gitlab"
	"gopkg.in/yaml.v3"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strings"
	"text/template"
)

var generateCmd = &cobra.Command{
	Use:  "generate",
	Args: cobra.ExactArgs(1),
	RunE: generateFunc,
}

//go:embed preamble.tpl.md
var preamble string

var preambleTpl = template.Must(template.New("preamble.md").Parse(preamble))

const (
	flagTemplate      = "template"
	flagFooter        = "footer"
	flagComponentPath = "component-path"
)

func init() {
	generateCmd.Flags().StringP(flagTemplate, "t", "", "path to the template file. Note: this is the template to use for the documentation, not the actual component definition.")
	generateCmd.Flags().String(flagFooter, "", "path to a file with contents that should be appended to the bottom of generated docs.")
	generateCmd.Flags().String(flagComponentPath, "gitlab.com/group/project/component@~latest", "component path to use in the 'getting started' description.")

	_ = generateCmd.MarkFlagRequired(flagTemplate)
	_ = generateCmd.MarkFlagRequired(flagComponentPath)
	_ = generateCmd.MarkFlagFilename(flagTemplate, ".md")
	_ = generateCmd.MarkFlagFilename(flagFooter, ".md")
}

type ComponentTemplate struct {
	Content string
	Footer  string
	Stages  []string
}

type PreambleTemplate struct {
	Stages         []string
	RequiredInputs map[string]string
	ComponentPath  string
}

func generateFunc(cmd *cobra.Command, args []string) error {
	component := args[0]
	tpl, _ := cmd.Flags().GetString(flagTemplate)
	footerPath, _ := cmd.Flags().GetString(flagFooter)
	componentPath, _ := cmd.Flags().GetString(flagComponentPath)

	log.Printf("generating docs for '%s' using '%s' as a template", component, tpl)

	var footer string
	if footerPath != "" {
		log.Printf("reading footer: %s", footerPath)
		f, err := os.ReadFile(footerPath)
		if err != nil {
			return fmt.Errorf("reading footer %s: %w", footerPath, err)
		}
		footer = string(f)
	}

	// read the component
	f, err := os.Open(filepath.Clean(component))
	if err != nil {
		return fmt.Errorf("opening component %s: %w", component, err)
	}

	// parse the component
	var c gitlab.Component
	if err := yaml.NewDecoder(f).Decode(&c); err != nil {
		return fmt.Errorf("decoding yaml: %w", err)
	}

	// read the template
	tmpl, err := template.ParseFiles(tpl)
	if err != nil {
		return fmt.Errorf("creating template from %s: %w", tpl, err)
	}

	sb := &strings.Builder{}

	// generate the table
	tw := tablewriter.NewWriter(sb)
	tw.SetHeader([]string{"Name", "Description", "Default value", "Required", "Constraints"})
	tw.SetAutoFormatHeaders(false)
	tw.SetBorders(tablewriter.Border{Left: true, Top: false, Right: true, Bottom: false})
	tw.SetCenterSeparator("|")
	tw.SetAutoWrapText(false)

	var stages []string
	requiredInputs := map[string]string{}

	// collect the list of keys and then alphabetically sort
	// them so that output is consistent
	keys := make([]string, 0, len(c.Spec.Inputs))
	for k := range c.Spec.Inputs {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		v := c.Spec.Inputs[k]
		var val string
		if v.Default != nil && !reflect.ValueOf(v.Default).IsZero() {
			if strings.HasPrefix(strings.ToLower(k), "stage") {
				log.Printf("found suspected stage in key '%s': %v", k, v.Default)
				stages = append(stages, fmt.Sprintf("%v", v.Default))
			}
			val = fmt.Sprintf("`%v`", v.Default)
		}
		log.Printf("found input '%s' with value '%s'", k, val)
		required := "No"
		if v.Default == nil {
			required = "Yes"
			log.Printf("found required input '%s'", k)
			requiredInputs[k] = "todo"
		}
		description := v.Description
		if description == "" {
			description = "*No description provided.*"
		}
		constraint := "*No constraints.*"
		if v.Regex != nil && *v.Regex != "" {
			constraint = fmt.Sprintf("Regex: `%s`", *v.Regex)
		}
		if v.Options != nil {
			constraint = "One of:<br/>"
			for _, o := range v.Options {
				constraint = fmt.Sprintf("%s*`%s`<br/>", constraint, o)
			}
		}
		tw.Append([]string{fmt.Sprintf("`%s`", k), description, val, required, constraint})
	}

	preOut := &bytes.Buffer{}
	preambleData := PreambleTemplate{
		Stages:         stages,
		ComponentPath:  componentPath,
		RequiredInputs: requiredInputs,
	}

	if err := preambleTpl.Execute(preOut, preambleData); err != nil {
		return fmt.Errorf("templating preamble: %w", err)
	}
	sb.WriteString(preOut.String())

	tw.Render()

	out := &bytes.Buffer{}

	data := ComponentTemplate{
		Content: sb.String(),
		Stages:  stages,
		Footer:  footer,
	}

	if err := tmpl.Execute(out, data); err != nil {
		return fmt.Errorf("templating: %w", err)
	}

	fmt.Println(out.String())
	return nil
}
