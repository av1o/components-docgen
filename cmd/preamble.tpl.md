## Usage

```yaml
stages:
{{- range $item := .Stages }}
  - {{ $item }}
{{- end }}

include:
  - component: {{ .ComponentPath }}
{{- if .RequiredInputs }}
    inputs:
    {{- range $k, $v := .RequiredInputs }}
      {{ $k }}: {{ $v }}
    {{- end }}
{{- end }}
```

## Input parameters

> This section has been automatically generated.
> If you need to make changes, make them to the component and regenerate the documentation.

This component provides several `inputs` that can be used to configure how it behaves.

