package cmd

import (
	"github.com/spf13/cobra"
	"os"
)

var command = &cobra.Command{
	Use:          "docgen",
	Short:        "Generates documentation for GitLab Components",
	SilenceUsage: true,
}

func init() {
	command.AddCommand(generateCmd)
}

func Execute(version string) {
	command.Version = version
	if err := command.Execute(); err != nil {
		os.Exit(1)
	}
}
