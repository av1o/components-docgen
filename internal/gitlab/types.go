package gitlab

type Component struct {
	Spec ComponentSpec `yaml:"spec"`
}

type ComponentSpec struct {
	Inputs map[string]Input `yaml:"inputs"`
}

type Input struct {
	Default     any      `yaml:"default"`
	Description string   `yaml:"description"`
	Options     []string `yaml:"options"`
	Regex       *string  `yaml:"regex"`
	Type        *Type    `yaml:"type"`
}

type Type string

const (
	TypeString  Type = "string"
	TypeArray   Type = "array"
	TypeNumber  Type = "number"
	TypeBoolean Type = "boolean"
)
